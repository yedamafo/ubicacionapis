<?php

session_start();
require './config/facebook.php';
require './vendor/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphUser;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;

FacebookSession::setDefaultApplication($config['app_id'],$config['app_secret']);
$helper= new FacebookRedirectLoginHelper('https://localhost/UbicacionApis/index.php'); //Ubicacion De la pagina

try {
	$session=$helper->getSessionFromRedirect();
	if ($session) {
		$_SESSION['facebook']=$session->getToken();
		header('Location: index.php');
	}
	if (isset($_SESSION['facebook'])) {
		$session = new FacebookSession($_SESSION['facebook']);
		$request = new FacebookRequest($session,'GET','/me');
		$response = $request->execute();
		$graphObject = $response->getGraphObject(GraphUser::className());
		$facebook_user=$graphObject;

	}
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

?>